<?php 

class CommonController extends MX_Controller{

	protected $data = array();

	function __construct(){
		parent::__construct();

		$this->_initTemplate();
	}


	protected function _initTemplate(){

		$this->template
                ->enable_parser(FALSE)
                ->set_theme('front-theme')
                ->set_layout('default')
                ->set_partial('head')
                ->set_partial('header')
                ->set_partial('footer');
	}

}

?>