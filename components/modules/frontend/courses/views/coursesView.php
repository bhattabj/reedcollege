</div><!-- container + head wrapper end -->
    
    <div id="k-body"><!-- content wrapper -->
    
        <div class="container"><!-- container -->
        
            <div class="row"><!-- row -->
            
                <div id="k-top-search" class="col-lg-12 clearfix"><!-- top search -->
                
                    <form action="#" id="top-searchform" method="get" role="search">
                        <div class="input-group">
                            <input type="text" name="s" id="sitesearch" class="form-control" autocomplete="off" placeholder="Type in keyword(s) then hit Enter on keyboard" />
                        </div>
                    </form>
                    
                    <div id="bt-toggle-search" class="search-icon text-center"><i class="s-open fa fa-search"></i><i class="s-close fa fa-times"></i></div><!-- toggle search button -->
                
                </div><!-- top search end -->
            
                <div class="k-breadcrumbs col-lg-12 clearfix"><!-- breadcrumbs -->
                
                    <ol class="breadcrumb">
                        <li><a href="index.html">Home</a></li>
                        <li class="active">Courses</li>
                    </ol>
                    
                </div><!-- breadcrumbs end -->               
                
            </div><!-- row end -->
            
            <div class="row no-gutter"><!-- row -->
                
                <div class="col-lg-8 col-md-8"><!-- doc body wrapper -->
                    
                    <div class="col-padded"><!-- inner custom column -->
                    
                        <div class="row gutter"><!-- row -->
                        
                            <div class="col-lg-12 col-md-12">
                    
                                <h1 class="page-title">Available Courses</h1><!-- category title -->
                            
                            </div>
                        
                        </div><!-- row end -->
                    
                        <div class="row gutter"><!-- row -->
                        
                            <div class="col-lg-12 col-md-12">
                                
                                <table class="table table-striped table-courses">
                                    <thead>
                                        <tr>
                                            <th>Course #</th>
                                            <th>Course Title</th>
                                            <th>Level</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>12BF34C</td>
                                            <td><a href="#" title="course title">Consectetur pulvinar lorem est</a></td>
                                            <td>Graduate</td>
                                        </tr>
                                        <tr>
                                            <td>2AER783</td>
                                            <td><a href="#" title="course title">Metus pellentesque nullam course</a></td>
                                            <td>Graduate</td>
                                        </tr>
                                        <tr>
                                            <td>RTY671W</td>
                                            <td><a href="#" title="course title">Varius elit ipsum dolorem</a></td>
                                            <td>Graduate</td>
                                        </tr>
                                        <tr>
                                            <td>4U7669Y</td>
                                            <td><a href="#" title="course title">Porttitor tempor ligula forte dolorum</a></td>
                                            <td>Undergraduate</td>
                                        </tr>
                                        <tr>
                                            <td>78AF11S</td>
                                            <td><a href="#" title="course title">Suscipit ligula est lorentis</a></td>
                                            <td>Graduate</td>
                                        </tr>
                                        <tr>
                                            <td>18SD1S</td>
                                            <td><a href="#" title="course title">Nunc lectus sapien venenatis et urna</a></td>
                                            <td>Undergraduate</td>
                                        </tr>
                                        <tr>
                                            <td>AAWE44</td>
                                            <td><a href="#" title="course title">Posuere pharetra lacus</a></td>
                                            <td>Graduate</td>
                                        </tr>
                                        <tr>
                                            <td>0092A4</td>
                                            <td><a href="#" title="course title">Ut rhoncus odio vel consectetur bibendum</a></td>
                                            <td>Undergraduate</td>
                                        </tr>
                                        <tr>
                                            <td>55TY12</td>
                                            <td><a href="#" title="course title">Aenean aliquam dui tellus</a></td>
                                            <td>Graduate</td>
                                        </tr>
                                        <tr>
                                            <td>UY76W3</td>
                                            <td><a href="#" title="course title">Eget convallis dui porta sit amet</a></td>
                                            <td>Graduate</td>
                                        </tr>
                                        <tr>
                                            <td>99OPAH</td>
                                            <td><a href="#" title="course title">Vivamus leo magna lobortis et nibh</a></td>
                                            <td>Undergraduate</td>
                                        </tr>
                                        <tr>
                                            <td>KL9114</td>
                                            <td><a href="#" title="course title">Condimentum vitae feugiat id</a></td>
                                            <td>Undergraduate</td>
                                        </tr>
                                        <tr>
                                            <td>TT1234</td>
                                            <td><a href="#" title="course title">Suspendisse vulputate diam quis tellus</a></td>
                                            <td>Undergraduate</td>
                                        </tr>
                                        <tr>
                                            <td>F6TP90</td>
                                            <td><a href="#" title="course title">Eget convallis dui porta sit amet</a></td>
                                            <td>Graduate</td>
                                        </tr>
                                        <tr>
                                            <td>V3V592</td>
                                            <td><a href="#" title="course title">Etiam pretium elit ac dolor tempus tristique</a></td>
                                            <td>Graduate</td>
                                        </tr>
                                    </tbody>
                                </table>
                            
                            </div>
                        
                        </div><!-- row end --> 
                        
                        <div class="row gutter"><!-- row -->
                        
                            <div class="col-lg-12">
                        
                                <ul class="pagination pull-right"><!-- pagination -->
                                    <li class="disabled"><a href="#">Prev</a></li>
                                    <li class="active"><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">4</a></li>
                                    <li><a href="#">5</a></li>
                                    <li><a href="#">Next</a></li>
                                </ul><!-- pagination end -->
                            
                            </div>
                            
                        </div><!-- row end -->                 
                    
                    </div><!-- inner custom column end -->
                    
                </div><!-- doc body wrapper end -->
                
                <div id="k-sidebar" class="col-lg-4 col-md-4"><!-- sidebar wrapper -->
                    
                    <div class="col-padded col-shaded"><!-- inner custom column -->
                    
                        <ul class="list-unstyled clear-margins"><!-- widgets -->
                        
                            <li class="widget-container widget_course_search"><!-- widget -->
                            
                                <h1 class="title-titan">Course Finder</h1>
                                
                                <form role="search" method="get" id="course-finder" action="#">
                                    <div class="input-group">
                                        <input type="text" placeholder="Find a course..." autocomplete="off" class="form-control" id="find-course" name="find-course" />
                                        <span class="input-group-btn"><button type="submit" class="btn btn-default">GO!</button></span>
                                    </div>
                                    <span class="help-block">* Enter course ID, title or the course instructor name</span>
                                </form>
                            
                            </li><!-- widget end -->
                            
                            <li class="widget-container widget_up_events"><!-- widgets list -->
                    
                                <h1 class="title-widget">Upcoming Events</h1>
                                
                                <ul class="list-unstyled">
                                
                                    <li class="up-event-wrap">
                                
                                        <h1 class="title-median"><a href="#" title="Annual alumni game">Annual alumni game</a></h1>
                                        
                                        <div class="up-event-meta clearfix">
                                            <div class="up-event-date">Jul 25, 2015</div><div class="up-event-time">9:00 - 11:00</div>
                                        </div>
                                        
                                        <p>
                                        Fusce condimentum pulvinar mattis. Nunc condimentum sapien sit amet odio vulputate, nec suscipit orci pharetra... <a href="#" class="moretag" title="read more">MORE</a> 
                                        </p>
                                    
                                    </li>
                                    
                                    <li class="up-event-wrap">
                                
                                        <h1 class="title-median"><a href="#" title="School talents gathering">School talents gathering</a></h1>
                                        
                                        <div class="up-event-meta clearfix">
                                            <div class="up-event-date">Aug 25, 2015</div><div class="up-event-time">8:30 - 10:30</div>
                                        </div>
                                        
                                        <p>
                                        Pellentesque lobortis, arcu eget condimentum auctor, magna neque faucibus dui, ut varius diam neque sed diam... <a href="#" class="moretag" title="read more">MORE</a> 
                                        </p>
                                    
                                    </li>
                                    
                                    <li class="up-event-wrap">
                                
                                        <h1 class="title-median"><a href="#" title="School talents gathering">Campus "Open Doors"</a></h1>
                                        
                                        <div class="up-event-meta clearfix">
                                            <div class="up-event-date">Sep 04, 2015</div><div class="up-event-date">Sep 11, 2015</div>
                                        </div>
                                        
                                        <p>
                                        Donec fringilla lacinia laoreet. Vestibulum ultrices blandit tempor. Aenean magna elit, varius eget quam a, posuere... <a href="#" class="moretag" title="read more">MORE</a> 
                                        </p>
                                    
                                    </li>
                                
                                </ul>
                            
                            </li><!-- widgets list end -->
                            
                            <li class="widget-container widget_text"><!-- widget -->
                            
                                <a href="#" class="custom-button cb-red" title="How to apply?">
                                    <i class="custom-button-icon fa fa-empire"></i>
                                    <span class="custom-button-wrap">
                                        <span class="custom-button-title">Donate Now</span>
                                        <span class="custom-button-tagline">Become a corporate sponsor of our schools!</span>
                                    </span>
                                    <em></em>
                                </a>
                            
                            </li>
                            
                        </ul><!-- widgets end -->
                    
                    </div><!-- inner custom column end -->
                    
                </div><!-- sidebar wrapper end -->
            
            </div><!-- row end -->
        
        </div><!-- container end -->
    
    </div><!-- content wrapper end -->
    